package com.ss.jenkinsdemo.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ss.jenkinsdemo.entity.Person;
import com.ss.jenkinsdemo.repository.PersonRepository;

@Service
public class PersonService {
	@Autowired
	private PersonRepository personRepository;
	private Logger logger=LoggerFactory.getLogger(getClass());
	public List<Person> getAllPersonDetail() {
		logger.info("Start:Get all person");
		List<Person> personList=null;
		try {
			personList=personRepository.findAll();
		}
		catch(Exception e) {
			
		}
		logger.info("End all person");
		return personList;

	}
	public void addPerson(Person person) {
		logger.debug("Start:add new Person detail");
		try {
		personRepository.save(person);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
	}

}
