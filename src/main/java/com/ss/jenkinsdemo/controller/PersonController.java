package com.ss.jenkinsdemo.controller;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ss.jenkinsdemo.entity.Person;
import com.ss.jenkinsdemo.service.PersonService;

@RestController
public class PersonController {
	private Logger logger=LoggerFactory.getLogger(getClass());
	@Autowired
	private PersonService personservice;
	@GetMapping(value="/getPerson")
	public ResponseEntity<?>getAllPerson(){
		List<Person>lstPerson=null;
		logger.info("getAllPerson");
		try {
			lstPerson	=personservice.getAllPersonDetail();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return ResponseEntity.status(HttpStatus.OK).body(lstPerson);
		
	}
	@PostMapping(value="/savePerson")
	public void savePerson(@RequestBody Person person){
		logger.info("getAllPerson");
		try {
			personservice.addPerson(person);
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	

}
